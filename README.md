# Random Procedural Core (rp-core) for Foundry VTT

**Current version: 1.0.7**

This is a library module for shared code, variables, and settings used by our other modules.

---

## Random Procedural Names

See [Random Procedural Names](https://gitlab.com/rpgm-tools/rp-names) for our name generator module, featuring AI name generation.

## Random Procedural Creations

See [Random Procedural Creations](https://gitlab.com/rpgm-tools/rp-creations) for our AI-powered homebrew creation module.
