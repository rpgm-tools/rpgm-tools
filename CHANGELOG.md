# Changelog

All notable changes to the "Random Procedural Core" module will be documented in this file.

## [1.0.7] - 2023-09-10

### Fixed

- Fixed an issue with gathering actor data for actors that have languages. This was causing some functions to simply not work because an error was
  being thrown.

## [1.0.6] - 2023-08-27

### Fixed

- Fixed erroneous error message about rp-names version mismatch
- Resolved merge issue

## [1.0.5] - 2023-08-27

### Fixed

- Fixed a bug that caused all chat messages to be whispered to the GM only
- Fixed a bug with naming a token from the !n/!name chat command
- Improved support for the Crucible system by Atropos and crew
- Minor improvement to settings migration

## [1.0.4] - 2023-08-26

### Fixed

- Added back in the missing Model setting

## [1.0.3] - 2023-08-19

### Added

- Added a version checker to make sure all modules are up-to-date. This will be used to notify users when there is a mismatch between versions.

### Fixed

- Restored a missing setting
- Filled in some missing localization strings for Brazilian Portuguese
- Added some error handling for variables

## [1.0.2] - 2023-08-18

### Fixed

- Misc. bug fixes

## [1.0.1] - 2023-08-17

### Fixed

- Added error checking for settings transfer.

## [1.0.0] - 2023-08-16

### Added

- Initial release of RPGM.tools core library module. This is now a dependency for Random Procedural Creations.
- Many of the core functions, settings, and variable from Random Procedural Creations have been moved to the core library module in order to make them
  available to other modules.
