const rp = CONFIG.rp;

const actorFunctions = {
	dnd5e_character: function dnd5e_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			race: actor.system?.details?.race ?? "",
			background: actor.system?.details?.background ?? "",
			originalClass: actor.system?.details?.originalClass ?? "",
			appearance: actor.system?.details?.appearance ?? "",
			trait: actor.system?.details?.trait ?? "",
			ideal: actor.system?.details?.ideal ?? "",
			bond: actor.system?.details?.bond ?? "",
			flaw: actor.system?.details?.flaw ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
			gender: actor.flags?.["tidy5e-sheet"]?.gender ?? actor.flags?.gender ?? "",
			age: actor.flags?.["tidy5e-sheet"]?.age ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	dnd5e_npc: function dnd5e_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			race: actor.system?.details?.race ?? "",
			creatureType: actor.system?.details?.type?.value ?? "",
			creatureSubtype: actor.system?.details?.type?.subtype ?? "",
			environment: actor.system?.details?.environment ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	dnd5e_vehicle: function dnd5e_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			capacity: actor.system?.attributes?.capacity ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			vehicleType: actor.system?.vehicleType ?? "",
			crew: actor.system?.cargo?.crew ?? [],
			passengers: actor.system?.cargo?.passengers ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	dnd5e_group: function dnd5e_group(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			descriptionFull: actor.system?.description?.full ?? "",
			descriptionSummary: actor.system?.description?.summary ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	pf2e_character: function pf2e_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.system?.details?.alignment?.value ?? "",
			appearance: actor.system?.details?.biography?.appearance ?? actor.system?.details?.appearance?.value ?? "",
			characterClass: actor.items?.find((item) => item.type === "class")?.name ?? "",
			backstory: actor.system?.details?.biography?.backstory ?? actor.system?.details?.backstory?.value ?? "",
			birthPlace: actor.system?.details?.biography?.birthPlace ?? "",
			attitude: actor.system?.details?.biography?.attitude ?? "",
			beliefs: actor.system?.details?.biography?.beliefs ?? "",
			likes: actor.system?.details?.biography?.likes ?? "",
			dislikes: actor.system?.details?.biography?.dislikes ?? "",
			catchphrases: actor.system?.details?.biography?.catchphrases ?? "",
			allies: actor.system?.details?.biography?.allies ?? "",
			enemies: actor.system?.details?.biography?.enemies ?? "",
			organizations: actor.system?.details?.biography?.organizations ?? "",
			age: actor.system?.details?.age?.value ?? "",
			gender: actor.system?.details?.gender?.value ?? actor.flags?.gender ?? "",
			ethnicity: actor.system?.details?.ethnicity?.value ?? "",
			nationality: actor.system?.details?.nationality?.value ?? "",
			traits: actor.system?.traits?.value ?? [],
			languages: actor.system?.traits?.languages?.value ?? [],
			heritage: actor.items?.find((item) => item.type === "heritage")?.name ?? "",
			ancestry: actor.items?.find((item) => item.type === "ancestry")?.name ?? "",
			size: actor.items?.find((item) => item.type === "ancestry")?.system?.size ?? "",
			edicts: actor.system?.details?.biography?.edicts ?? [],
			anathema: actor.system?.details?.biography?.anathema ?? [],
			personality: actor.system?.details?.biography?.attitude ?? actor.system?.details?.biography?.beliefs ?? "",
		};
	
		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	pf2e_npc: function pf2e_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.system?.details?.alignment?.value ?? "",
			creatureType: actor.system?.details?.creatureType ?? "",
			blurb: actor.system?.details?.blurb ?? "",
			publicNotes: actor.system?.details?.publicNotes ?? "",
			privateNotes: actor.system?.details?.privateNotes ?? "",
			traits: actor.system?.traits?.value ?? [],
			languages: actor.system?.traits?.languages?.value ?? [],
			gender: actor.flags?.gender ?? "",
			size: actor.system?.traits?.size?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	pf2e_vehicle: function pf2e_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.details?.description ?? "",
			space: actor.system?.details?.space ?? "",
			crew: actor.system?.details?.crew ?? "",
			passengers: actor.system?.details?.passengers ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	pf2e_familiar: function pf2e_familiar(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			creatureType: actor.system?.details?.creature?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	pf2e_hazard: function pf2e_hazard(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			creatureType: actor.system?.creatureType ?? "",
			description: actor.system?.details?.description ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	pf2e_loot: function pf2e_loot(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.details?.description ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sfrpg_character: function sfrpg_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			characterClass: actor.items?.find((item) => item.type === "class")?.name || actor.system?.details?.class || "",
			theme: actor.items?.find((item) => item.type === "theme")?.name ?? "",
			race: actor.items?.fing((item) => item.type === "race")?.name || actor.system?.details?.race || "",
			creatureType: actor.items?.fing((item) => item.type === "race")?.system?.type ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			age: actor.system?.details?.biography?.age ?? "",
			deity: actor.system?.details?.biography?.deity || actor.system?.details?.deity || "",
			genderPronouns: actor.system?.details?.biography?.genderPronouns ?? "",
			homeworld: actor.system?.details?.biography?.homeworld ?? "",
			organization: actor.system?.details?.organization ?? "",
			theme: actor.system?.details?.theme ?? "",
			environment: actor.system?.details?.environment ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sfrpg_npc2: function sfrpg_npc2(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.system?.details?.alignment ?? "",
			race: actor.system?.details?.race ?? "",
			characterClass: actor.system?.details?.class ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			age: actor.system?.details?.biography?.age ?? "",
			deity: actor.system?.details?.biography?.deity ?? "",
			genderPronouns: actor.system?.details?.biography?.genderPronouns ?? "",
			homeworld: actor.system?.details?.biography?.homeworld ?? "",
			creatureType: actor.system?.details?.type ?? "",
			environment: actor.system?.details?.environment ?? "",
			organization: actor.system?.details?.organization ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sfrpg_npc: function sfrpg_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.system?.details?.alignment ?? "",
			race: actor.system?.details?.race ?? "",
			characterClass: actor.system?.details?.class ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			age: actor.system?.details?.biography?.age ?? "",
			deity: actor.system?.details?.biography?.deity ?? "",
			genderPronouns: actor.system?.details?.biography?.genderPronouns ?? "",
			homeworld: actor.system?.details?.biography?.homeworld ?? "",
			creatureType: actor.system?.details?.type ?? "",
			environment: actor.system?.details?.environment ?? "",
			organization: actor.system?.details?.organization ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sfrpg_drone: function sfrpg_drone(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.system?.details?.alignment ?? "",
			race: actor.system?.details?.race ?? "",
			characterClass: actor.system?.details?.class ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			age: actor.system?.details?.biography?.age ?? "",
			deity: actor.system?.details?.biography?.deity || actor.system?.details?.deity || "",
			genderPronouns: actor.system?.details?.biography?.genderPronouns ?? "",
			homeworld: actor.system?.details?.biography?.homeworld ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sfrpg_vehicle: function sfrpg_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.details?.description?.value ?? "",
			size: actor.system?.attributes?.size ?? "",
			vehicleType: actor.system?.attributes?.vehicleType ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sfrpg_starship: function sfrpg_starship(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			model: actor.system?.details?.model ?? "",
			notes: actor.system?.details?.notes ?? "",
			size: actor.system?.details?.size ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sfrpg_hazard: function sfrpg_hazard(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.details?.description?.value ?? "",
			hazardType: actor.system?.details?.type ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	wwn_character: function wwn_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.details?.biography?.value ?? "",
			characterClass: actor.system?.details?.class ?? "",
			background: actor.system?.details?.background ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	wwn_monster: function wwn_monster(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.details?.biography ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	CoC7_character: function CoC7_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			occupation: actor.system?.infos?.occupation ?? "",
			age: actor.system?.infos?.age ?? "",
			gender: actor.system?.infos?.sex ?? actor.flags?.gender ?? "",
			residence: actor.system?.infos?.residence ?? "",
			birthplace: actor.system?.infos?.birthplace ?? "",
			archetype: actor.system?.infos?.archetype ?? "",
			organization: actor.system?.infos?.organization ?? "",
			biography: actor.biography ?? [],
			sanityLossEvents: actor.sanityLossEvents ?? [],
			backstory: actor.backstory ?? "",
			description: actor.system?.description?.keeper ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	CoC7_npc: function CoC7_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			personalDescription: actor.system?.personalDescription?.value ?? "",
			description: actor.system?.description?.keeper ?? "",
			occupation: actor.system?.infos?.occupation ?? "",
			age: actor.system?.infos?.age ?? "",
			gender: actor.system?.infos?.sex ?? actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	CoC7_creature: function CoC7_creature(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			personalDescription: actor.system?.personalDescription?.value ?? "",
			description: actor.system?.description?.keeper ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	CoC7_container: function CoC7_container(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	"cyberpunk-red-core_character": function cyberpunkRed_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			activeRole: actor.system?.roleInfo?.activeRole ?? "",
			activeNetRoleAlias: actor.system?.roleInfo?.activeNetRole ?? "",
			description: actor.system?.information?.description ?? "",
			history: actor.system?.information?.history ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	"cyberpunk-red-core_mook": function cyberpunkRed_mook(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			activeRole: actor.system?.roleInfo?.activeRole ?? "",
			activeNetRoleAlias: actor.system?.roleInfo?.activeNetRole ?? "",
			description: actor.system?.information?.description ?? "",
			history: actor.system?.information?.history ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	"cyberpunk-red-core_blackIce": function cyberpunkRedCore_blackIce(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			characterClass: actor.system?.class ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	"cyberpunk-red-core_demon": function cyberpunkRed_demon(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	"cyberpunk-red-core_container": function cyberpunkRedCore_container(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			containerClass: actor.system?.items.length > 0 ? actor.system?.items[0]?.data?.data?.class ?? "" : "",
			reinforced: actor.system?.reinforced ?? false,
			restricted: actor.system?.restricted ?? false,
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	D35E_character: function D35E_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.system?.details?.alignment ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			gender: actor.system?.details?.gender ?? actor.flags?.gender ?? "",
			deity: actor.system?.details?.deity ?? "",
			age: actor.system?.details?.age ?? "",
			size: actor.system?.traits?.size ?? "",
			incorporeal: actor.system?.details?.incorporeal ?? false,
			languages: actor.system?.traits?.languages?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	D35E_npc: function D35E_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.system?.details?.alignment ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			creatureType: actor.system?.details?.type ?? "",
			environment: actor.system?.details?.environment ?? "",
			size: actor.system?.traits?.size ?? "",
			incorporeal: actor.system?.details?.incorporeal ?? false,
			languages: actor.system?.traits?.languages?.value ?? [],
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	D35E_object: function D35E_object(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.details?.alignment ?? "",
			biography: actor.details?.biography?.value ?? "",
			biographyPublic: actor.details?.biography?.public ?? "",
			creatureType: actor.system?.details?.type ?? "",
			environment: actor.system?.details?.environment ?? "",
			size: actor.system?.traits?.size ?? "",
			incorporeal: actor.details?.incorporeal ?? false,
			languages: actor.system?.traits?.languages?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	D35E_trap: function D35E_trap(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			alignment: actor.details?.alignment ?? "",
			biography: actor.details?.biography?.value ?? "",
			biographyPublic: actor.details?.biography?.public ?? "",
			creatureType: actor.system?.details?.type ?? "",
			environment: actor.system?.details?.environment ?? "",
			size: actor.system?.traits?.size ?? "",
			incorporeal: actor.details?.incorporeal ?? false,
			languages: actor.system?.traits?.languages?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	"dnd4e_Player Character": function dnd4e_PlayerCharacter(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.biography ?? "",
			characterClass: actor.system?.details?.class ?? "",
			paragon: actor.system?.details?.paragon ?? "",
			race: actor.system?.details?.race ?? "",
			size: actor.system?.details?.size ?? "",
			origin: actor.system?.details?.origin ?? "",
			creatureType: actor.system?.details?.type ?? "",
			age: actor.system?.details?.age ?? "",
			gender: actor.system?.details?.gender ?? actor.flags?.gender ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			deity: actor.system?.details?.deity ?? "",
			languages: actor.system?.languages?.spoken ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	dnd4e_NPC: function dnd4e_NPC(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.biography ?? "",
			characterClass: actor.system?.details?.class ?? "",
			paragon: actor.system?.details?.paragon ?? "",
			race: actor.system?.details?.race ?? "",
			size: actor.system?.details?.size ?? "",
			origin: actor.system?.details?.origin ?? "",
			creatureType: actor.system?.details?.type ?? "",
			age: actor.system?.details?.age ?? "",
			gender: actor.system?.details?.gender ?? actor.flags?.gender ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			deity: actor.system?.details?.deity ?? "",
			languages: actor.system?.languages?.spoken ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	shadowrun5e_character: function shadowrun5e_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			metatype: actor.system?.metatype ?? "",
			special: actor.system?.special ?? "",
			description: actor.system?.description?.value ?? "",
			ethnicity: actor.system?.ethnicity ?? "",
			age: actor.system?.age ?? "",
			gender: actor.system?.sex ?? actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	shadowrun5e_critter: function shadowrun5e_critter(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			metatype: actor.system?.metatype ?? "",
			special: actor.system?.special ?? "",
			description: actor.system?.description?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	shadowrun5e_spirit: function shadowrun5e_spirit(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			metatype: actor.system?.metatype ?? "",
			special: actor.system?.special ?? "",
			description: actor.system?.description?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	shadowrun5e_sprite: function shadowrun5e_sprite(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			metatype: actor.system?.metatype ?? "",
			special: actor.system?.special ?? "",
			description: actor.system?.description?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	shadowrun5e_vehicle: function shadowrun5e_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			metatype: actor.system?.metatype ?? "",
			special: actor.system?.special ?? "",
			description: actor.system?.description?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sw5e_character: function sw5e_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			description: actor.system?.details?.description?.value ?? "",
			descriptionPublic: actor.system?.details?.description?.public ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			species: actor.system?.details?.species ?? "",
			creatureType: actor.system?.details?.type?.value ?? "",
			creatureSubtype: actor.system?.details?.type?.subtype ?? "",
			creatureCustomType: actor.system?.details?.type?.custom ?? "",
			environment: actor.system?.details?.environment ?? "",
			background: actor.system?.details?.background ?? "",
			originalClass: actor.system?.details?.originalClass ?? "",
			trait: actor.system?.details?.trait ?? "",
			ideal: actor.system?.details?.ideal ?? "",
			bond: actor.system?.details?.bond ?? "",
			flaw: actor.system?.details?.flaw ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sw5e_npc: function sw5e_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			description: actor.system?.details?.description?.value ?? "",
			descriptionPublic: actor.system?.details?.description?.public ?? "",
			alignment: actor.system?.details?.alignment ?? "",
			species: actor.system?.details?.species ?? "",
			creatureType: actor.system?.details?.type?.value ?? "",
			creatureSubtype: actor.system?.details?.type?.subtype ?? "",
			creatureCustomType: actor.system?.details?.type?.custom ?? "",
			environment: actor.system?.details?.environment ?? "",
			size: actor.system?.traits?.size ?? "",
			languages: actor.system?.traits?.languages?.value ?? [],
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sw5e_vehicle: function sw5e_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			description: actor.system?.details?.description?.value ?? "",
			descriptionPublic: actor.system?.details?.description?.public ?? "",
			vehicleType: actor.system?.vehicleType ?? "",
			cargo: actor.system?.cargo?.crew ?? [],
			passengers: actor.system?.cargo?.passengers ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	sw5e_group: function sw5e_group(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			descriptionFull: actor.system?.description?.full ?? "",
			descriptionSummary: actor.system?.description?.summary ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swade_character: function swade_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			archetype: actor.system?.details?.archetype ?? "",
			appearance: actor.system?.details?.appearance ?? "",
			goals: actor.system?.details?.goals ?? "",
			biography: actor.system?.details?.biography ?? "",
			species: actor.system?.details?.species ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swade_npc: function swade_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			archetype: actor.system?.details?.archetype ?? "",
			appearance: actor.system?.details?.appearance ?? "",
			goals: actor.system?.details?.goals ?? "",
			biography: actor.system?.details?.biography ?? "",
			species: actor.system?.details?.species ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swade_vehicle: function swade_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			classification: actor.system?.classification ?? "",
			description: actor.system?.description ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swnr_character: function swnr_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			characterClass: actor.system?.class ?? "",
			species: actor.system?.species ?? "",
			homeworld: actor.system?.homeworld ?? "",
			background: actor.system?.background ?? "",
			employer: actor.system?.employer ?? "",
			biography: actor.system?.biography ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swnr_npc: function swnr_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			species: actor.system?.species ?? "",
			homeworld: actor.system?.homeworld ?? "",
			faction: actor.system?.faction ?? "",
			goals: actor.system?.notes?.left?.contents ?? "",
			biography: actor.system?.notes?.right?.contents ?? "",
			biographyPublic: actor.system?.notes?.public?.contents ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swnr_mech: function swnr_mech(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			mechClass: actor.system?.mechClass ?? "",
			mechHullType: actor.system?.mechHullType ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swnr_faction: function swnr_faction(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			homeworld: actor.system?.homeworld ?? "",
			factionGoal: actor.system?.factionGoal ?? "",
			factionGoalDesc: actor.system?.factionGoalDesc ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swnr_vehicle: function swnr_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			size: actor.system?.size ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swnr_ship: function swnr_ship(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			shipClass: actor.system?.shipClass ?? "",
			shipHullType: actor.system?.shipHullType ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	swnr_drone: function swnr_drone(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			model: actor.system?.model ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	t2k4e_character: function t2k4e_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			nationality: actor.system?.bio?.nationality ?? "",
			branch: actor.system?.bio?.branch ?? "",
			militaryRank: actor.system?.bio?.militaryRank ?? "",
			buddy: actor.system?.bio?.buddy ?? "",
			appearance: actor.system?.bio?.appearance ?? "",
			moralCode: actor.system?.bio?.moralCode ?? "",
			bigDream: actor.system?.bio?.bigDream ?? "",
			age: actor.system?.bio?.age ?? "",
			description: actor.system?.description ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	t2k4e_npc: function t2k4e_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	t2k4e_unit: function t2k4e_unit(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			info: actor.system?.info ?? "",
			faction: actor.system?.faction ?? "",
			unitAffiliation: actor.system?.unitAffiliation ?? "",
			unitSize: actor.system?.unitSize ?? "",
			unitType: actor.system?.unitType ?? "",
			unitModifiers: Object.keys(actor.system?.unitModifiers).length === 0 ? actor.system?.unitModifiers : {},
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	t2k4e_party: function t2k4e_party(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	t2k4e_vehicle: function t2k4e_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			description: actor.system?.description ?? "",
			vehicleType: actor.system?.vehicleType ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	wfrp4e_character: function wfrp4e_character(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			species: actor.system?.details?.species?.value ?? "",
			subspecies: actor.system?.details?.species?.subspecies ?? "",
			gender: actor.system?.details?.gender?.value ?? actor.flags?.gender ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			gmnotes: actor.system?.details?.gmnotes?.value ?? "",
			size: actor.system?.traits?.size?.value ?? "",
			god: actor.system?.traits?.god?.value ?? "",
			"personal-ambitions": actor.system?.traits["personal-ambitions"]?.value ?? "",
			"party-ambitions": actor.system?.traits["party-ambitions"]?.value ?? "",
			motivation: actor.system?.traits?.motivation?.value ?? "",
			characterClass: actor.system?.traits?.class?.value ?? "",
			career: actor.system?.traits?.career?.value ?? "",
			careerlevel: actor.system?.traits?.careerlevel?.value ?? "",
			age: actor.system?.traits?.age?.value ?? "",
			distinguishingmarks: actor.system?.traits?.distinguishingmarks?.value ?? "",
			starsign: actor.system?.traits?.starsign?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	wfrp4e_npc: function wfrp4e_npc(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			species: actor.system?.details?.species?.value ?? "",
			subspecies: actor.system?.details?.species?.subspecies ?? "",
			gender: actor.system?.details?.gender?.value ?? actor.flags?.gender ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			gmnotes: actor.system?.details?.gmnotes?.value ?? "",
			size: actor.system?.traits?.size?.value ?? "",
			god: actor.system?.traits?.god?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	wfrp4e_creature: function wfrp4e_creature(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			species: actor.system?.details?.species?.value ?? "",
			subspecies: actor.system?.details?.species?.subspecies ?? "",
			gender: actor.system?.details?.gender?.value ?? actor.flags?.gender ?? "",
			biography: actor.system?.details?.biography?.value ?? "",
			gmnotes: actor.system?.details?.gmnotes?.value ?? "",
			size: actor.system?.traits?.size?.value ?? "",
			god: actor.system?.traits?.god?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	wfrp4e_vehicle: function wfrp4e_vehicle(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			vehicleType: actor.system?.details?.vehicletype?.value ?? "",
			description: actor.system?.details?.description?.value ?? "",
			gmdescription: actor.system?.details?.gmdescription?.value ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	crucible_hero: function crucible_hero(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			ancestry: actor.system?.details?.ancestry ?? "",
			background: actor.system?.details?.background ?? "",
			biography: actor.system?.details?.biography?.private ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	crucible_adversary: function crucible_adversary(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			archetype: actor.system?.details?.archetype ?? "",
			size: actor.system?.details?.stature ?? "",
			taxonomy: actor.system?.details?.taxonomy ?? "",
			threat: actor.system?.details?.threat ?? "",
			biography: actor.system?.details?.biography?.private ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default_character: function default_character(actor) {
		let extracted = {
			activeNetRoleAlias: actor.system?.roleInfo?.activeNetRole ?? "",
			activeRole: actor.system?.roleInfo?.activeRole ?? "",
			age:
				actor.flags?.["tidy5e-sheet"]?.age ||
				actor.system?.bio?.age ||
				actor.system?.details?.age ||
				actor.system?.traits?.age?.value ||
				actor.system?.age ||
				actor.system?.infos?.age ||
				actor.system?.details?.biography?.age ||
				"",
			alignment: actor.system?.details?.alignment || actor.system?.details?.alignment?.value || "",
			allies: actor.system?.details?.biography?.allies ?? "",
			ancestry: actor.items?.find((item) => item?.type === "ancestry")?.name ?? "",
			appearance: actor.system?.bio?.appearance || actor.system?.details?.appearance || actor.system?.details?.biography?.appearance || "",
			archetype: actor.system?.details?.archetype || actor.system?.infos?.archetype || "",
			attitude: actor.system?.details?.biography?.attitude ?? "",
			background: actor.system?.details?.background || actor.system?.background || "",
			backstory: actor.backstory || actor.system?.details?.biography?.backstory || "",
			beliefs: actor.system?.details?.biography?.beliefs ?? "",
			bigDream: actor.system?.bio?.bigDream ?? "",
			biography: actor.biography || actor.system?.details?.biography || actor.system?.details?.biography?.value || actor.system?.biography || [],
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			birthPlace: actor.system?.details?.biography?.birthPlace ?? "",
			birthplace: actor.system?.infos?.birthplace ?? "",
			bond: actor.system?.details?.bond ?? "",
			branch: actor.system?.bio?.branch ?? "",
			buddy: actor.system?.bio?.buddy ?? "",
			career: actor.system?.traits?.career?.value ?? "",
			careerlevel: actor.system?.traits?.careerlevel?.value ?? "",
			catchphrases: actor.system?.details?.biography?.catchphrases ?? "",
			characterClass:
				actor.system?.traits?.class?.value ||
				actor.system?.class ||
				actor.system?.details?.class ||
				actor.items?.find((item) => item.type === "class")?.name ||
				"",
			creature: actor.name,
			creatureCustomType: actor.system?.details?.type?.custom ?? "",
			creatureSubtype: actor.system?.details?.type?.subtype ?? "",
			creatureType: actor.system?.details?.type?.value || actor.items?.fing((item) => item.type === "race")?.system?.type || "",
			deity: actor.system?.details?.deity || actor.system?.details?.biography?.deity || "",
			description:
				actor.system?.details?.description?.value ||
				actor.system?.information?.description ||
				actor.system?.description ||
				actor.system?.description?.keeper ||
				actor.system?.description?.value ||
				"",
			descriptionPublic: actor.system?.details?.description?.public ?? "",
			dislikes: actor.system?.details?.biography?.dislikes ?? "",
			distinguishingmarks: actor.system?.traits?.distinguishingmarks?.value ?? "",
			employer: actor.system?.employer ?? "",
			enemies: actor.system?.details?.biography?.enemies ?? "",
			environment: actor.system?.details?.environment ?? "",
			ethnicity: actor.system?.details?.ethnicity?.value || actor.system?.ethnicity || "",
			flaw: actor.system?.details?.flaw ?? "",
			gender:
				actor.flags?.["tidy5e-sheet"]?.gender ||
				actor.flags?.gender ||
				actor.system?.details?.gender ||
				actor.system?.details?.gender?.value ||
				actor.system?.infos?.sex ||
				actor.system?.sex ||
				"",
			genderPronouns: actor.system?.details?.biography?.genderPronouns ?? "",
			gmnotes: actor.system?.details?.gmnotes?.value ?? "",
			goals: actor.system?.details?.goals ?? "",
			god: actor.system?.traits?.god?.value ?? "",
			heritage: actor.items?.find((item) => item?.type === "heritage")?.name ?? "",
			history: actor.system?.information?.history ?? "",
			homeworld: actor.system?.homeworld || actor.system?.details?.biography?.homeworld || "",
			ideal: actor.system?.details?.ideal ?? "",
			incorporeal: actor.system?.details?.incorporeal ?? false,
			languages: actor.system?.traits?.languages?.value ?? [],
			likes: actor.system?.details?.biography?.likes ?? "",
			metatype: actor.system?.metatype ?? "",
			militaryRank: actor.system?.bio?.militaryRank ?? "",
			moralCode: actor.system?.bio?.moralCode ?? "",
			motivation: actor.system?.traits?.motivation?.value ?? "",
			nationality: actor.system?.bio?.nationality || actor.system?.details?.nationality?.value || "",
			occupation: actor.system?.infos?.occupation ?? "",
			organization: actor.system?.infos?.organization || actor.system?.details?.organization || "",
			organizations: actor.system?.details?.biography?.organizations ?? "",
			originalClass: actor.system?.details?.originalClass ?? "",
			"party-ambitions": actor.system?.traits["party-ambitions"]?.value ?? "",
			"personal-ambitions": actor.system?.traits["personal-ambitions"]?.value ?? "",
			race: actor.system?.details?.race || actor.items?.fing((item) => item.type === "race")?.name || "",
			residence: actor.system?.infos?.residence ?? "",
			sanityLossEvents: actor.sanityLossEvents ?? [],
			size:
				actor.items?.find((item) => item?.type === "ancestry")?.system?.size || actor.system?.traits?.size || actor.system?.traits?.size?.value || "",
			special: actor.system?.special ?? "",
			species: actor.system?.details?.species || actor.system?.details?.species?.value || actor.system?.species || "",
			starsign: actor.system?.traits?.starsign?.value ?? "",
			subspecies: actor.system?.details?.species?.subspecies ?? "",
			theme: actor.items?.find((item) => item.type === "theme")?.name || actor.system?.details?.theme || "",
			trait: actor.system?.details?.trait ?? "",
			traits: actor.system?.traits?.value ?? [],
			type: actor.type,
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default_creature: function default_creature(actor) {
		let extracted = {
			biography: actor.system?.details?.biography?.value ?? "",
			creature: actor.name,
			description: actor.system?.description?.keeper ?? "",
			gender: actor.system?.details?.gender?.value ?? actor.flags?.gender ?? "",
			gmnotes: actor.system?.details?.gmnotes?.value ?? "",
			god: actor.system?.traits?.god?.value ?? "",
			personalDescription: actor.system?.personalDescription?.value ?? "",
			size: actor.system?.traits?.size?.value ?? "",
			species: actor.system?.details?.species?.value ?? "",
			subspecies: actor.system?.details?.species?.subspecies ?? "",
			type: actor.type,
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default_container: function default_container(actor) {
		let extracted = {
			containerClass: actor.system?.items?.length > 0 ? actor.system?.items[0]?.data?.data?.class ?? "" : "",
			creature: actor.name,
			description: actor.system?.description?.value ?? "",
			reinforced: actor.system?.reinforced ?? false,
			restricted: actor.system?.restricted ?? false,
			type: actor.type,
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default_critter: function default_critter(actor) {
		let extracted = {
			creature: actor.name,
			description: actor.system?.description?.value ?? "",
			metatype: actor.system?.metatype ?? "",
			special: actor.system?.special ?? "",
			type: actor.type,
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default_monster: function default_monster(actor) {
		let extracted = {
			alignment: actor.system?.details?.alignment ?? "",
			biography: actor.system?.details?.biography ?? "",
			creature: actor.name,
			type: actor.type,
			gender: actor.flags?.gender ?? "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default_npc: function default_npc(actor) {
		let extracted = {
			age: actor.system?.infos?.age || actor.system?.bio?.age || actor.system?.details?.age || "",
			alignment: actor.system?.details?.alignment?.value || actor.system?.details?.alignment || "",
			appearance: actor.system?.details?.appearance ?? "",
			archetype: actor.system?.details?.archetype ?? "",
			biography: actor.system?.details?.biography?.value || actor.system?.details?.biography || actor.system?.notes?.right?.contents || "",
			biographyPublic: actor.system?.details?.biography?.public || actor.system?.notes?.public?.contents || "",
			blurb: actor.system?.details?.blurb ?? "",
			creature: actor.name,
			creatureCustomType: actor.system?.details?.type?.custom ?? "",
			creatureSubtype: actor.system?.details?.type?.subtype ?? "",
			creatureType: actor.system?.details?.creatureType || actor.system?.details?.type?.value || actor.system?.details?.type || "",
			description: actor.system?.details?.description?.value || actor.system?.description || actor.system?.description?.keeper || "",
			descriptionPublic: actor.system?.details?.description?.public ?? "",
			environment: actor.system?.details?.environment ?? "",
			faction: actor.system?.faction ?? "",
			gender: actor.system?.details?.gender?.value || actor.system?.infos?.sex || actor.flags?.gender || "",
			gmnotes: actor.system?.details?.gmnotes?.value ?? "",
			goals: actor.system?.details?.goals || actor.system?.notes?.left?.contents || "",
			god: actor.system?.traits?.god?.value ?? "",
			homeworld: actor.system?.homeworld ?? "",
			incorporeal: actor.system?.details?.incorporeal ?? false,
			languages: actor.system?.traits?.languages?.value ?? [],
			occupation: actor.system?.infos?.occupation ?? "",
			personalDescription: actor.system?.personalDescription?.value ?? "",
			privateNotes: actor.system?.details?.privateNotes ?? "",
			publicNotes: actor.system?.details?.publicNotes ?? "",
			race: actor.system?.details?.race ?? "",
			size: actor.system?.traits?.size?.value || actor.system?.traits?.size || "",
			species: actor.system?.details?.species?.value || actor.system?.details?.species || actor.system?.species || "",
			subspecies: actor.system?.details?.species?.subspecies ?? "",
			traits: actor.system?.traits?.value ?? [],
			type: actor.type,
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default_vehicle: function default_vehicle(actor) {
		let extracted = {
			biography: actor.system?.details?.biography?.value ?? "",
			biographyPublic: actor.system?.details?.biography?.public ?? "",
			capacity: actor.system?.attributes?.capacity ?? "",
			cargo: actor.system?.cargo?.crew ?? [],
			classification: actor.system?.classification ?? "",
			creature: actor.name,
			crew: actor.system?.details?.crew ?? "",
			description: actor.system?.description || actor.system?.details?.description?.value || actor.system?.details?.description?.public || "",
			gmdescription: actor.system?.details?.gmdescription?.value ?? "",
			metatype: actor.system?.metatype ?? "",
			passengers: actor.system?.cargo?.passengers || actor.system?.details?.passengers || [],
			size: actor.system?.size ?? "",
			space: actor.system?.details?.space ?? "",
			special: actor.system?.special ?? "",
			type: actor.type,
			vehicleType: actor.system?.details?.vehicletype?.value || actor.system?.vehicleType || "",
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
	default: function default_actor(actor) {
		let extracted = {
			creature: actor.name,
			type: actor.type,
			age: actor.system?.infos?.age || actor.system?.bio?.age || actor.system?.details?.age || "",
			alignment: actor.system?.details?.alignment?.value || actor.system?.details?.alignment || "",
			appearance: actor.system?.details?.appearance ?? "",
			archetype: actor.system?.details?.archetype ?? "",
			biography: actor.system?.details?.biography?.value || actor.system?.details?.biography || actor.system?.notes?.right?.contents || "",
			biographyPublic: actor.system?.details?.biography?.public || actor.system?.notes?.public?.contents || "",
			blurb: actor.system?.details?.blurb ?? "",
			creatureCustomType: actor.system?.details?.type?.custom ?? "",
			creatureSubtype: actor.system?.details?.type?.subtype ?? "",
			creatureType: actor.system?.details?.creatureType || actor.system?.details?.type?.value || actor.system?.details?.type || "",
			description: actor.system?.details?.description?.value || actor.system?.description || actor.system?.description?.keeper || "",
			descriptionPublic: actor.system?.details?.description?.public ?? "",
			environment: actor.system?.details?.environment ?? "",
			faction: actor.system?.faction ?? "",
			gender: actor.system?.details?.gender?.value || actor.system?.infos?.sex || actor.flags?.gender || "",
			gmnotes: actor.system?.details?.gmnotes?.value ?? "",
			goals: actor.system?.details?.goals || actor.system?.notes?.left?.contents || "",
			god: actor.system?.traits?.god?.value ?? "",
			homeworld: actor.system?.homeworld ?? "",
			incorporeal: actor.system?.details?.incorporeal ?? false,
			languages: actor.system?.traits?.languages?.value ?? [],
			occupation: actor.system?.infos?.occupation ?? "",
			personalDescription: actor.system?.personalDescription?.value ?? "",
			privateNotes: actor.system?.details?.privateNotes ?? "",
			publicNotes: actor.system?.details?.publicNotes ?? "",
			race: actor.system?.details?.race ?? "",
			size: actor.system?.traits?.size?.value || actor.system?.traits?.size || "",
			species: actor.system?.details?.species?.value || actor.system?.details?.species || actor.system?.species || "",
			subspecies: actor.system?.details?.species?.subspecies ?? "",
			traits: actor.system?.traits?.value ?? [],
		};

		rp.deleteEmptyProperties(extracted);
		return extracted;
	},
};

function formatValue(value) {
	if (typeof value === "string") {
		return `"${value}"`;
	}
	if (Array.isArray(value)) {
		return `[${value.map((v) => formatValue(v)).join(", ")}]`;
	}
	if (value instanceof Set) {
		return `[${[...value].map((v) => formatValue(v)).join(", ")}]`;
	}
	if (typeof value === "object") {
		return JSON.stringify(value);
	}
	return value;
}

async function getActorData(system, actor, name, custom) {
	const defaultKey = `default_${actor.type}`;
	const key = `${system}_${actor.type}`;
	let actorData = {};

	if (actorFunctions[key]) {
		actorData = actorFunctions[key](actor);
	} else if (actorFunctions[defaultKey]) {
		actorData = actorFunctions[defaultKey](actor);
	} else {
		actorData = actorFunctions.default(actor);
	}

	if (actorData.alignment) {
		actorData.alignment = rp.unabbreviate(actorData.alignment, "alignment");
	}
	if (actorData.size) {
		actorData.size = rp.unabbreviate(actorData.size, "size");
	}

	if (typeof actorData.biography === "string" && /forgotten/i.test(actorData.biography)) {
		delete actorData.biography;
	}

	if (Array.isArray(actorData.languages)) {
		const index = actorData.languages.indexOf("custom");
		if (index > -1) {
			actorData.languages.splice(index, 1);
		}
		if (!actorData.languages.includes("Common")) {
			actorData.languages.push("Common");
		}
	} else {
		actorData.languages = ["Common"];
	}

	actorData.name = name || "";

	if (custom) {
		try {
			await new Promise((resolve, reject) => {
				let content = `<strong>${game.i18n.localize("RPGM-TOOLS.ACTORS.DIALOG_PROMPT")}:</strong><br /><form style='line-height: 2;'>`;
				for (let key in actorData) {
					content += `<div style='display: flex; align-items: center;'>
                                    <input type="checkbox" name="${key}" checked style='margin-right: 5px;'>
                                    <span>${key}: ${formatValue(actorData[key])}</span>
                                </div>`;
				}
				content += "</form>";

				new Dialog({
					title: `${game.i18n.localize("RPGM-TOOLS.ACTORS.DIALOG_TITLE")}`,
					content: content,
					buttons: {
						ok: {
							label: `${game.i18n.localize("RPGM-TOOLS.ACTORS.DIALOG_SUBMIT")}`,
							callback: (html) => {
								for (let key in actorData) {
									if (!html.find(`input[name="${key}"]`).is(":checked")) {
										delete actorData[key];
									}
								}
								resolve();
							},
						},
					},
					default: "ok",
					close: () => reject(`${game.i18n.localize("RPGM-TOOLS.ACTORS.DIALOG_CANCEL")}`),
				}).render(true);
			});

			rp.dev("Actor Data:\n", actorData);
			return actorData;
		} catch (err) {
			rp.warn(err);
			return false;
		}
	} else {
		rp.dev("Actor Data:\n", actorData);
		return actorData;
	}
}

export { getActorData };
