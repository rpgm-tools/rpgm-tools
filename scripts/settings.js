const rp = CONFIG.rp;

async function registerSettings() {
	const schemas = rp.lists.homebrewSchemas.reduce((acc, schema) => {
		acc[schema.recordType] = schema;
		return acc;
	}, {});

	const defaultGenre = getDefaultGenre(game.system.id);

	rp.genreChoices = getLocalizedChoices(rp.lists.genres, 'label');
	rp.languageChoices = getLocalizedChoices(rp.lists.languages);

	registerGameSettings(defaultGenre, schemas);
}

function getDefaultGenre(systemId) {
	switch (systemId) {
		case "CoC7": return "Lovecraftian";
		case "cyberpunk-red-core":
		case "shadowrun5e":
		case "shadowrun6": return "Cyberpunk";
		case "wwn":
		case "swnr":
		case "sfrpg": return "Futuristic";
		case "sw5e":
		case "starwarsffg": return "Star Wars";
		case "t2k4e": return "Modern";
		default: return "Fantasy";
	}
}

function getLocalizedChoices(list, labelKey = 'value') {
	return list.reduce((obj, item) => {
		obj[item.value] = game.i18n.localize(item[labelKey]);
		return obj;
	}, {});
}

function registerGameSettings(defaultGenre, schemas) {
	const settings = [
		{
			key: "settingsPatreonKey",
			name: "RPGM-TOOLS.SETTINGS.PATREON_KEY",
			hint: "RPGM-TOOLS.SETTINGS.PATREON_KEY_HINT",
			type: String,
			default: "",
		},
		{
			key: "settingsGenre",
			name: "RPGM-TOOLS.SETTINGS.GENRE",
			hint: "RPGM-TOOLS.SETTINGS.GENRE_HINT",
			type: String,
			default: defaultGenre,
		},
		{
			key: "settingsLanguage",
			name: "RPGM-TOOLS.SETTINGS.LANGUAGE",
			hint: "RPGM-TOOLS.SETTINGS.LANGUAGE_HINT",
			type: String,
			default: "Default",
		},
		{
			key: "settingsModel",
			name: "RPGM-TOOLS.SETTINGS.MODEL",
			hint: "RPGM-TOOLS.SETTINGS.MODEL_HINT",
			type: String,
			default: "gpt-3.5-turbo",
			choices: getModelChoices,
		},
		{
			key: "settingsTemperature",
			name: "RPGM-TOOLS.SETTINGS.TEMPERATURE",
			hint: "RPGM-TOOLS.SETTINGS.TEMPERATURE_HINT",
			type: Number,
			default: 1,
			range: { min: 0, max: 1.8, step: 0.1 },
		},
		{
			key: "settingsApiKey",
			name: "RPGM-TOOLS.SETTINGS.API_KEY",
			hint: "RPGM-TOOLS.SETTINGS.API_KEY_HINT",
			type: String,
			default: "",
		},
		{
			key: "settingsDevMode",
			name: "RPGM-TOOLS.SETTINGS.DEVELOPER_MODE",
			hint: "RPGM-TOOLS.SETTINGS.DEVELOPER_MODE_HINT",
			type: Boolean,
			default: false,
		},
		{
			key: "settingsHomebrewSchemas",
			name: "Homebrew Schemas",
			type: Object,
			default: schemas,
			config: false,
		},
		{
			key: "settingsClientId",
			name: "Client ID",
			type: String,
			default: "",
			config: false,
			scope: "client",
		},
		{
			key: "settingsMigrated",
			name: "Settings Migrated",
			type: Boolean,
			default: false,
			config: false,
			requiresReload: true,
		}
	];

	settings.forEach(setting => {
		game.settings.register("rpgm-tools", setting.key, {
			name: game.i18n.localize(setting.name),
			hint: game.i18n.localize(setting.hint),
			scope: setting.scope || "world",
			config: setting.config !== undefined ? setting.config : true,
			default: setting.default,
			type: setting.type,
			restricted: true,
			choices: setting.choices || undefined,
			range: setting.range || undefined,
			requiresReload: setting.requiresReload || false,
		});
	});
}

function getModelChoices() {
	const choices = { "gpt-3.5-turbo": "gpt-3.5-turbo" };
	const apiKey = game.settings.get("rpgm-tools", "settingsApiKey");
	if (apiKey && apiKey.length === 51 && apiKey.startsWith("sk-")) {
		choices["gpt-3.5-turbo-16k"] = "gpt-3.5-turbo-16k";
		choices["gpt-4"] = "gpt-4";
	}
	return choices;
}

export { registerSettings };
