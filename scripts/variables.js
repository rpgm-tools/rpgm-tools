import { descMap } from "./commonEarly.js";
const rp = CONFIG.rp;

const apiKey = game?.settings?.get("rpgm-tools", "settingsApiKey") || "";
const patreonKey = game?.settings?.get("rpgm-tools", "settingsPatreonKey") || "";
const clientId = game?.settings?.get("rpgm-tools", "settingsClientId") || rp.getClientId() || "";
const model = apiKey ? game.settings.get("rpgm-tools", "settingsModel") : "gpt-3.5-turbo";
const system = game?.system?.id || "dnd5e";
const temperature = game.settings.get("rpgm-tools", "settingsTemperature") || 1;
const genre = game?.settings?.get("rpgm-tools", "settingsGenre") || "fantasy";
const language = game?.settings?.get("rpgm-tools", "settingsLanguage") || "Default";

export { apiKey, patreonKey, clientId, model, system, temperature, genre, language, descMap };
