import { lowercaseWords } from "./lists.js";
const descMap = new Map();

function toTitleCase(str) {
	const lowercaseWordsSet = new Set(lowercaseWords);

	return str
		.split(/\b/)
		.map((word, index) => {
			if (index === 0 || !lowercaseWordsSet.has(word.toLowerCase())) {
				return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
			}
			return word.toLowerCase();
		})
		.join("");
}

async function createJournalEntry(name, content) {
	let folder = game.folders.find((f) => f.name === "RPGM-Tools" && f.type === "JournalEntry");

	if (!folder) {
		folder = await Folder.create({
			name: "RPGM-Tools",
			type: "JournalEntry",
			parent: null,
		});
	}

	return JournalEntry.create({
		name: name,
		content: content,
		folder: folder.id,
	});
}

export { toTitleCase, createJournalEntry, descMap };
