const rp = CONFIG.rp;

const chatColors = {
	default: "#FFFFFF",
	help: {
		background: "#CCCCFF",
		border: "#0033FF",
		text: "#0033FF",
		background_x: "#000000",
		text_x: "#88CCFF",
		link: "#0022AA",
	},
	error: {
		background: "#FFCCCC",
		border: "#FF0000",
		text: "#FF0000",
	},
	name: {
		background: "#CCEEFF",
		border: "#115588",
		text: "#115588",
	},
	homebrew: {
		background: "#CCCCFF",
		border: "#0033FF",
		text: "#0033FF",
	},
	gpt: {
		background: "#CCCCFF",
		border: "#0033FF",
		text: "#0033FF",
	},
};

async function createChatMessage(content, whisper = [game.user.id]) {
	return ChatMessage.create({ content, whisper });
}

async function addAutoDelete(chatMessage, countdown, color) {
	const chatContent = chatMessage.content;
	const timer = setInterval(async () => {
		countdown--;
		if (!game.messages.has(chatMessage.id)) return clearInterval(timer);
		if (countdown < 0) return clearInterval(timer), await chatMessage.delete();
		await chatMessage.update({
			content: `${chatContent}<p style="color: ${color};">${game.i18n.localize("RPGM-TOOLS.CHAT.CONTROLS.AUTODELETE")}: ${countdown}</p>`,
		});
		ui.chat.scrollBottom();
	}, 1000);
}

async function chatHelp() {
	const savedSchemas = game.settings.get("rpgm-tools", "settingsHomebrewSchemas");
	const recordTypeKeywords = Object.values(savedSchemas)
		.sort((a, b) => a.recordType.localeCompare(b.recordType))
		.map((schema) => schema.recordType)
		.join("\n");
	let content = `
        <div class="ai-content" style="border:2px solid ${chatColors.help.border}; color: ${chatColors.help.text}; background-color: ${
		chatColors.help.background
	}; padding: 10px; border-radius: 5px;">
            <h2>RPGM.tools ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HELP")}</h2>
            <p>${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.COMMANDS")}:</p>
            <ul>
                ${rp.generateName ? generateNameHelp() : ""}
                ${rp.homebrew ? homebrewHelp(recordTypeKeywords) : ""}
                ${rp.gptPrompt ? gptHelp() : ""}
                <li><b>!help</b> - ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HELP_COMMAND")}</li>
            </ul>
            <p>${game.i18n.localize(
							"RPGM-TOOLS.CHAT.HELP.FOOTNOTE_1"
						)} <a href="https://gitlab.com/rpgm-tools/rpgm-tools/-/blob/main/INSTRUCTIONS.md" style="font-weight: bold; color: ${
		chatColors.help.link
	};" target="_blank">${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.FOOTNOTE_2")}</a> ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.FOOTNOTE_3")}</p>
        </div>`;

	let chatMessage = await createChatMessage(content, [game.user.id]);
	await chatMessage.setFlag("rpgm-tools", "copyText", rp.extractAIContentFromString(content));
	await chatMessage.update({
		content: `${content}<br><div style="text-align: center;"><button style="background-color: ${chatColors.help.background}; color: ${
			chatColors.help.text
		}; border: 1px solid ${chatColors.help.border}; padding: 2; cursor: pointer; width: auto; line-height: 1;" class="delete-card" data-message-id="${
			chatMessage._id
		}">${game.i18n.localize("RPGM-TOOLS.CHAT.CONTROLS.DELETE")}</button></div>`,
	});
	addAutoDelete(chatMessage, 60, chatColors.help.text);
}

function generateNameHelp() {
	return `
		<li>
			<b>!n</b> | <b>!name</b> + ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.NAME_INSTRUCTIONS_1")}<br />
			<div style="background-color: ${chatColors.help.background_x}; padding: 5px;">
				<code style="color: ${chatColors.help.text_x};">!n ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.NAME_EXAMPLE_1")}</code><br />
				<code style="color: ${chatColors.help.text_x};">!n ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.NAME_EXAMPLE_2")}</code><br />
				<code style="color: ${chatColors.help.text_x};">!n ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.NAME_EXAMPLE_3")}</code><br />
			</div>
			<p>${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.NAME_ASSIGN")}</p>
		</li>`;
}

function homebrewHelp(recordTypeKeywords) {
	return `
		<li>
			<b>!h</b> | <b>!home</b> - ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HOME_INSTRUCTIONS_1")}<br />
			<b>!h</b> | <b>!home</b> + ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HOME_INSTRUCTIONS_2")}<br />
			<b>!h</b> | <b>!home</b> + <span style="color: red; font-weight: bold; cursor: pointer; text-decoration: underline;" title="${recordTypeKeywords}">${game.i18n.localize(
		"RPGM-TOOLS.CHAT.HELP.HOME_INSTRUCTIONS_3_1"
	)}</span> + ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HOME_INSTRUCTIONS_3_2")}
			<div style="background-color: ${chatColors.help.background_x}; padding: 5px;">
				<code style="color: ${chatColors.help.text_x};">!h</code><br />
				<code style="color: ${chatColors.help.text_x};">!h ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HOME_EXAMPLE_1")}</code><br />
				<code style="color: ${chatColors.help.text_x};">!h ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HOME_EXAMPLE_2")}</code><br />
				<code style="color: ${chatColors.help.text_x};">!h ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HOME_EXAMPLE_3")}</code><br />
			</div>
			<p>${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.HOME_ASSIGN")}</p>
		</li>`;
}

function gptHelp() {
	return `
		<li>
			<b>!g</b> | <b>!gpt</b> + ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.GPT_INSTRUCTIONS_1")}
			<div style="background-color: ${chatColors.help.background_x}; padding: 5px;">
				<code style="color: ${chatColors.help.text_x};">!g ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.GPT_EXAMPLE_1")}</code><br />
				<code style="color: ${chatColors.help.text_x};">!g ${game.i18n.localize("RPGM-TOOLS.CHAT.HELP.GPT_EXAMPLE_2")}</code><br />
			</div>
		</li>`;
}

async function sendError(errorMessage) {
	const helpText = errorMessage.includes("Usage") ? `<p><b>!help</b> ${game.i18n.localize("RPGM-TOOLS.CHAT.ERROR.USAGE")}.</p>` : "";
	const content = `<div class="rpgm-tools error" style="border:2px solid ${chatColors.error.border}; color: ${chatColors.error.text}; background-color: ${chatColors.error.background}; padding: 10px; border-radius: 5px;">${errorMessage}${helpText}</div>`;
	let chatMessage = await createChatMessage(content, [game.user.id]);
	addAutoDelete(chatMessage, 20, chatColors.error.text);
}

export { chatColors, chatHelp, sendError };
