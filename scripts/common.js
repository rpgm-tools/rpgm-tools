const rp = CONFIG.rp;

const messageHistory = [];
const maxMessageHistory = 10;
const minMessageInterval = 5000;
const maxUIMessageLength = 100;

function shouldShortenForUI(message) {
	return message.length > maxUIMessageLength;
}

function _recordMessage(message) {
	const now = Date.now();
	messageHistory.push({ message, time: now });
	if (messageHistory.length > maxMessageHistory) {
		messageHistory.shift();
	}
}

function _sendMessage(method, showUI, style, ...messages) {
	const now = Date.now();
	const { strings, objects } = messages.reduce((acc, msg) => {
		typeof msg === "string" ? acc.strings.push(msg) : acc.objects.push(msg);
		return acc;
	}, { strings: [], objects: [] });

	const formattedMessage = `%c${strings.join(" ")}`;
	const similarMessage = messageHistory.find((msg) => msg.message === formattedMessage && now - msg.time < minMessageInterval);

	if (!similarMessage) {
		console[method](formattedMessage, style, ...objects);
		_recordMessage(formattedMessage);
		if (showUI && game.user.isGM) showNotification(method, formattedMessage);
	}
}

function showNotification(method, formattedMessage) {
	let uiMessage = formattedMessage.replace(/%c/g, "");
	if (shouldShortenForUI(uiMessage)) uiMessage = "Message is too long. Check the console log for details.";
	ui.notifications[method](uiMessage);
}

function sendLogMessage(method, emoji, color, ...messages) {
	const showUI = messages[messages.length - 1] === true;
	if (showUI) messages.pop();
	messages.unshift(`${emoji} RPGM.tools |`);
	_sendMessage(method, showUI, `color: ${color}; font-weight: bold;`, ...messages);
}

function log(...messages) {
	sendLogMessage("log", "📜", "#ad8cef", ...messages);
}

function warn(...messages) {
	sendLogMessage("warn", "⚔️", "#7b4ed4", ...messages);
}

function error(...messages) {
	sendLogMessage("error", "💀", "#e93d98", ...messages);
}

function dev(...messages) {
	if (game?.settings?.get("rpgm-tools", "settingsDevMode")) {
		sendLogMessage("log", "🧪", "#ff0088", ...messages);
	}
}

function startJitter() {
	let headerD20Image = document.getElementById("header-d20-image");
	headerD20Image.style.display = "block";
	headerD20Image.classList.add("jitter");
	log(game.i18n.localize("RPGM-TOOLS.COMMON.JITTER_ON"));
}

function stopJitter() {
	let headerD20Image = document.getElementById("header-d20-image");
	headerD20Image.style.display = "none";
	headerD20Image.classList.remove("jitter");
	log(game.i18n.localize("RPGM-TOOLS.COMMON.JITTER_OFF"));
}

function getLocalStorageItem(name) {
	return localStorage.getItem(name);
}

function setLocalStorageItem(name, value) {
	localStorage.setItem(name, value);
}

function findNameByType(array, type) {
	const item = array.find((item) => item.type === type);
	return item ? item.name : "";
}

function getClientId() {
	const clientIdKey = "client-id";
	let clientId = getLocalStorageItem(clientIdKey) || generateUUID();
	setLocalStorageItem(clientIdKey, clientId);
	return clientId;
}

function getGender(token) {
    let src = token.document?.texture?.src?.toLowerCase();

    if (!src) {
        return getRandomGender();
    }

    if (src.includes("girl") || src.includes("female") || src.includes("(f)") || src.includes("[f]")) {
        return "Female";
    } else if (src.includes("boy") || src.includes("male") || src.includes("(m)") || src.includes("[m]")) {
        return "Male";
    }

    return getRandomGender();
}

function getRandomGender() {
    const genders = ["Male", "Female", "Non-Binary"];
    return genders[Math.floor(Math.random() * genders.length)];
}

function deleteLocalStorageItem(key) {
	localStorage.removeItem(key);
}

function deleteAllLocalStorageItems() {
	localStorage.clear();
}

function isOnline() {
	return navigator.onLine;
}

function randBetween(min, max) {
	const randomNumber = Math.floor(Math.random() * (max - min + 1) + min);
	return randomNumber;
}

function generateUUID() {
	const UUID = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
		let r = (Math.random() * 16) | 0,
			v = c === "x" ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
	return UUID;
}

function getOrNull(object, path) {
	const result = path.split('.').reduce((o, p) => o ? o[p] : undefined, object);
	return result === undefined ? "" : result;
}

function getItemName(items, type) {
	const item = items.find((item) => item.type === type);
	return item ? item.name : "";
}

function escapeDoubleQuotes(str) {
	return str.replace(/"/g, '\\"');
}

function addNestedValue(object, path, value) {
	const pathArray = path.split(".");
	const lastKey = pathArray.pop();
	let currentObj = object;

	for (const key of pathArray) {
		if (!currentObj.hasOwnProperty(key) || typeof currentObj[key] !== "object") {
			currentObj[key] = {};
		}
		currentObj = currentObj[key];
	}

	currentObj[lastKey] = value;
}

function getNestedProperty(obj, path) {
	let properties = path.split(".");
	let result = obj;

	for (let property of properties) {
		if (result === undefined) return "";
		result = result[property];
	}

	return result;
}

function deepEqual(obj1, obj2) {
	const keys1 = Object.keys(obj1).sort();
	const keys2 = Object.keys(obj2).sort();

	if (keys1.length !== keys2.length) return false;

	for (let key of keys1) {
		if (key !== "customSchema" && key !== "system") {
			const val1 = obj1[key];
			const val2 = obj2[key];

			const areObjects = typeof val1 === "object" && val1 !== null && typeof val2 === "object" && val2 !== null;

			if ((areObjects && !deepEqual(val1, val2)) || (!areObjects && val1 !== val2)) {
				return false;
			}
		}
	}

	return true;
}

function deleteEmptyProperties(object) {
	for (let key in object) {
		if (object[key] === "" || object[key] === null || (Array.isArray(object[key]) && object[key].length === 0)) {
			delete object[key];
		}
	}
	return object;
}

function extractAIContentFromString(str) {
	let parser = new DOMParser();
	let doc = parser.parseFromString(str, "text/html");
	let aiContentDiv = doc.querySelector(".ai-content");

	return aiContentDiv ? aiContentDiv.innerHTML : null;
}

function toTitleCase(str) {
	const lowercaseWordsSet = new Set(rp.lists.lowercaseWords);

	return str
		.split(/(\W+)/)
		.map((word, index) => {
			if (word.match(/^\W+$/)) return word;

			if (index === 0 || !lowercaseWordsSet.has(word.toLowerCase())) {
				return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
			}
			return word.toLowerCase();
		})
		.join("");
}

function debounce(func, wait) {
	let timeout;
	return (...args) => {
		const context = this;
		if (timeout) clearTimeout(timeout);
		timeout = setTimeout(() => func.apply(context, args), wait);
	};
}

function mergeSchemas() {
	const settingsHomebrewSchemas = game.settings.get("rpgm-tools", "settingsHomebrewSchemas");

	const fileSchemas = rp.lists.homebrewSchemas.reduce((acc, schema) => {
		acc[schema.recordType] = schema;
		return acc;
	}, {});

	let mergedSchemas = { ...fileSchemas };

	for (const key in settingsHomebrewSchemas) {
		const schemaObj = settingsHomebrewSchemas[key];
		schemaObj.system = rp.variables.system;
		mergedSchemas[key] = schemaObj;
	}

	for (const key in mergedSchemas) {
		if (!isNaN(key)) {
			delete mergedSchemas[key];
		}
	}

	game.settings.set("rpgm-tools", "settingsHomebrewSchemas", mergedSchemas);

	rp.dev("Merged Schemas:\n", mergedSchemas);
	return mergedSchemas;
}

function isValidObject(obj) {
	return typeof obj === "object" && obj !== null;
}

function generateGptPrompt({
	model = "gpt-3.5-turbo",
	messages = [
		{ role: "system", content: "You are The Brain." },
		{ role: "user", content: "What we going to do tonight, Brain?" },
		{ role: "assistant", content: "Concise response:\n\n" },
	],
	temperature = 1,
	presence_penalty = 0.2,
	frequency_penalty = 0.2,
} = {}) {
	return {
		model,
		messages,
		temperature,
		presence_penalty,
		frequency_penalty,
		apiKey: rp.variables.apiKey,
		patreonKey: rp.variables.patreonKey,
		clientId: rp.variables.clientId,
	};
}

function checkModuleVersions() {
	const expectedVersions = {
		"rpgm-tools": rp.coreVersion,
		"rp-creations": rp.creationsVersion,
		//"rp-names": rp.namesVersion,
	};

	const moduleNames = {
		"rpgm-tools": "Random Procedural Core",
		"rp-creations": "Random Procedural Creations",
		//"rp-names": "Random Procedural Names",
	};

	const moduleDescriptions = {
		"rp-creations": "Install or activate it now to generate homebrew content for your world using AI!",
		//"rp-names": "Install or activate it now to create custom names for your tokens using AI!",
	};

	for (let moduleId in expectedVersions) {
		const moduleData = game.modules.get(moduleId);
		const moduleName = moduleNames[moduleId];

		if (moduleData && moduleData.active) {
			const installedVersion = moduleData.version;
			const expectedVersion = expectedVersions[moduleId];

			if (installedVersion === expectedVersion) {
				rp.log(`${moduleName} is up-to-date with version ${installedVersion}.`);
			} else {
				rp.warn(`${moduleName} must be updated to ${expectedVersion} to function properly.`, true);
			}
		} else {
			const status = moduleData ? "disabled" : "not installed";
			if (moduleDescriptions[moduleId]) {
				rp.log(`${moduleName} is ${status}. ${moduleDescriptions[moduleId]}`);
			} else {
				rp.log(`${moduleName} is ${status}.`);
			}
		}
	}
}

function unabbreviate(input, type) {
	const sizeMapping = {
		tiny: "tiny",
		tin: "tiny",
		t: "tiny",
		small: "small",
		sml: "small",
		sm: "small",
		s: "small",
		medium: "medium",
		med: "medium",
		md: "medium",
		m: "medium",
		large: "large",
		lrg: "large",
		lar: "large",
		lg: "large",
		l: "large",
		huge: "huge",
		hug: "huge",
		hg: "huge",
		h: "huge",
		gargantuan: "gargantuan",
		garg: "gargantuan",
		gar: "gargantuan",
		g: "gargantuan",
	};

	const alignmentMapping = {
		lg: "lawful good",
		ln: "lawful neutral",
		le: "lawful evil",
		ng: "neutral good",
		nn: "true neutral",
		n: "true neutral",
		ne: "neutral evil",
		cg: "chaotic good",
		cn: "chaotic neutral",
		ce: "chaotic evil",
	};

	const mappings = { size: sizeMapping, alignment: alignmentMapping };

	return mappings[type][input.toLowerCase()] || input;
}

function getAiSettings(model, systemMessage, userMessage, assistantMessage, temperature, presencePenalty, frequencyPenalty) {
	const messages = [
		{ role: "system", text: systemMessage || "You are an all-knowing AI GM for TTRPGs." },
		{ role: "user", text: userMessage },
	];

	if (assistantMessage) {
		messages.push({ role: "assistant", text: assistantMessage });
	}

	return {
		model: rp.variables.model,
		messages,
		temperature: rp.variables.temperature,
		presence_penalty: presencePenalty || 0,
		frequency_penalty: frequencyPenalty || 0,
	};
}

async function getJSON(file) {
	let r = await fetch(file);
	let data = await r.json();
	return data;
}

const TIMEOUT = 60000;

async function callLambdaFunction(payload) {
	const url = "https://7r5j1szsn5.execute-api.us-west-1.amazonaws.com/gpt-35-turbo";

	if (!payload.patreonKey) {
		payload.patreonKey = rp.variables.patreonKey || "";
	}

	dev("Payload:\n", payload);

	for (let i = 0; i < 3; i++) {
		startJitter();

		try {
			log(game.i18n.localize("RPGM-TOOLS.COMMON.AI_EXPECTATIONS"));

			const response = await Promise.race([
				fetch(url, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify(payload),
				}),
				new Promise((_, reject) => setTimeout(() => reject(new Error("Request timed out")), TIMEOUT)),
			]);

			if (!response.ok && response.status === 503) {
				throw new Error("503 Service Unavailable");
			}

			const data = await response.json();
			dev("Response:\n", data);
			const validationMethod = data.validationMethod;
			switch (validationMethod) {
				case "apiKey":
					log(game.i18n.localize("RPGM-TOOLS.COMMON.VALIDATION.PERSONAL"));
					break;
				case "free":
					log(game.i18n.localize("RPGM-TOOLS.COMMON.VALIDATION.FREE"));
					break;
				case "check":
					log(game.i18n.localize("RPGM-TOOLS.COMMON.VALIDATION.PATREON"));
					break;
				default:
					log(game.i18n.localize("RPGM-TOOLS.COMMON.VALIDATION.UNKNOWN"));
			}
			return data.message;
		} catch (error) {
			if (i === 2 || error.message !== "503 Service Unavailable") {
				if (error.name === "TypeError") {
					error("Network Error");
				}
				warn(game.i18n.localize("RPGM-TOOLS.COMMON.FAIL"), true);
				dev(error);
				return error.message;
			} else if (error.message === "503 Service Unavailable") {
				warn("503 Service Unavailable. Retrying...", true);
			}
		} finally {
			stopJitter();
		}
	}
}

export {
	log,
	warn,
	error,
	dev,
	startJitter,
	stopJitter,
	getLocalStorageItem,
	setLocalStorageItem,
	findNameByType,
	getClientId,
	getGender,
	deleteLocalStorageItem,
	deleteAllLocalStorageItems,
	isOnline,
	getJSON,
	randBetween,
	generateUUID,
	getOrNull,
	getItemName,
	escapeDoubleQuotes,
	addNestedValue,
	getNestedProperty,
	deepEqual,
	deleteEmptyProperties,
	extractAIContentFromString,
	generateGptPrompt,
	checkModuleVersions,
	unabbreviate,
	toTitleCase,
	debounce,
	mergeSchemas,
	getAiSettings,
	callLambdaFunction,
};
