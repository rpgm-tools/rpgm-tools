let rp;

import { registerRenderSidebarTabHook, registerRenderChatMessageHook } from "./hooksEarly.js";
registerRenderSidebarTabHook();
registerRenderChatMessageHook();

async function initializeImports() {
	const init = await import("./init.js");
	rp = init.rp;
	rp.version = "1.0.0";

	try {
		const lists = await import("./lists.js");
		rp.lists = lists;

		const settings = await import("./settings.js");
		await settings.registerSettings();

		const common = await import("./common.js");
		Object.assign(rp, common);

		const variables = await import("./variables.js");
		rp.variables = variables;

		Hooks.call("rpReady");
	} catch (error) {
		rp.error ? rp.error("Error loading the RPGM.tools core:\n", error, true) : console.error("Error importing common.js:", error);
	}
}

Hooks.once("rpReady", async () => {
	const actors = await import("./actors.js");
	Object.assign(rp, actors);

	const chat = await import("./chat.js");
	Object.assign(rp, chat);

	const classes = await import("./classes.js");
	Object.assign(rp, classes);

	const handlebars = await import("./handlebars.js");
	handlebars.initializeHandlebars();

	const hooks = await import("./hooks.js");
	await hooks.registerChatMessageHook();

	const names = await import("./names.js");
	rp.names = names;

	rp.log("Core Ready");
});

Hooks.once("ready", async () => {
	await initializeImports();

	//	const hooks = await import("./hooks.js");

	console.log(
		`%cRPGM.tools v${rp.version}
________________________________________________
 ____  ____   ____ __  __  _              _     
|  _ \\|  _ \\ / ___|  \\/  || |_ ___   ___ | |___ 
| |_) | |_) | |  _| |\\/| || __/ _ \\ / _ \\| / __|
|  _ <|  __/| |_| | |  | || || (_) | (_) | \\__ \\
|_| \\_\\_|    \\____|_|  |_(_)__\\___/ \\___/|_|___/
________________________________________________`,
		"color: #d4428b; font-weight: bold;"
	);

	game.socket.on("module.rpgm-tools", (data) => {
		if (data.command.startsWith("!delete-card")) {
			let id = data.command.split(" ")[1];
			let msg = game.messages.get(id);
			if (msg.user.id === game.user.id || game.user.isGM) {
				msg.delete();
			}
		}
	});
});

export { rp };
