import { toTitleCase, createJournalEntry, descMap } from "./commonEarly.js";

function registerRenderSidebarTabHook() {
	Hooks.on("renderSidebarTab", async (app, html) => {
		const img = document.createElement("img");
		Object.assign(img, {
			id: "header-d20-image",
			src: "./modules/rpgm-tools/media/d20.webp",
			alt: "Processing AI Request",
			style: "display: none; border: none; width: 50%; position: absolute; right: 100%; top: 0;",
		});

		const sidebar = html[0].closest("#sidebar");
		if (sidebar) {
			sidebar.appendChild(img);
		}
	});
}

function registerRenderChatMessageHook() {
	Hooks.on("renderChatMessage", (chatItem, html, msg) => {
		html.find(".delete-card").click(handleDeleteCardClick);
		html.find(".create-journal").click(handleCreateJournalClick);
	});
}

function handleDeleteCardClick(ev) {
	const messageId = $(ev.currentTarget).data("message-id");
	const message = game.messages.get(messageId);
	if (message.user.id === game.user.id || game.user.isGM) {
		message.delete();
	} else {
		ui.notifications.warn(game.i18n.localize("RPGM-TOOLS.HOOKSEARLY.DELETE"));
	}
}

async function handleCreateJournalClick(ev) {
	const messageId = $(ev.currentTarget).data("message-id");
	const subject = toTitleCase($(ev.currentTarget).data("subject"));
	const descId = $(ev.currentTarget).data("desc-id");
	const desc = descMap.get(descId);
	const message = game.messages.get(messageId);

	if (!desc) {
		ui.notifications.warn(game.i18n.localize("RPGM-TOOLS.HOOKSEARLY.JOURNAL_1"));
	} else if (message.user.id === game.user.id || game.user.isGM) {
		const entry = await createJournalEntry(subject, desc);
		entry.sheet.render(true);
		message.delete();
	} else {
		ui.notifications.warn(game.i18n.localize("RPGM-TOOLS.HOOKSEARLY.JOURNAL_2"));
	}
}

export { registerRenderSidebarTabHook, registerRenderChatMessageHook };
