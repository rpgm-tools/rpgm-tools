const rp = CONFIG.rp;

async function registerRenderSettingsConfigHook() {
	Hooks.on("renderSettingsConfig", async (app, [elem], options) => {
		const moduleSettings = elem.querySelector('div[data-tab="rpgm-tools"]');
		if (moduleSettings) {
			const targetElement = moduleSettings.querySelector("h2.border");
			if (targetElement) {
				targetElement.insertAdjacentHTML(
					"beforebegin",
					`
                    <h1 style="text-align:center;">RPGM.tools</h1>
                    <hr>
                    `
				);
			}
		}
	});
}

async function registerChatMessageHook() {
	Hooks.on("chatMessage", (log, messageContent, data) => {
		const [command, ...args] = messageContent.split(" ");
		const schemas = rp.mergeSchemas();
		const recordTypesLowerCase = Object.values(schemas)
			.map((schema) => schema.recordType.toLowerCase())
			.sort();

		data.whisper = [game.user.id]; // Ensure this is an array

		const commands = {
			"!help": () => {
				rp.chatHelp();
				return false;
			},
			"!n": () => handleNameCommand(args, data),
			"!name": () => handleNameCommand(args, data),
			"!h": () => handleHomebrewCommand(args, recordTypesLowerCase, data),
			"!home": () => handleHomebrewCommand(args, recordTypesLowerCase, data),
			"!g": () => handleGptCommand(args, data),
			"!gpt": () => handleGptCommand(args, data),
		};

		return commands[command] ? commands[command]() : true;
	});
}

function handleNameCommand(args, data) {
	if (!rp.generateName) return true;
	if (args.length < 1) {
		if (canvas.tokens.controlled.length === 1) {
			rp.generateName(canvas.tokens.controlled[0].actor.name, data);
			return false;
		}
		rp.chatHelp();
		return false;
	}
	rp.generateName(args.join(" "), data);
	return false;
}

function handleHomebrewCommand(args, recordTypesLowerCase, data) {
	if (!rp.homebrew) return true;
	if (args.length < 1) {
		rp.homebrewDialog(data);
		return false;
	}
	const recordType = args[0].toLowerCase();
	if (recordTypesLowerCase.includes(recordType)) {
		args.shift();
		rp.homebrewArg(recordType, args.join(" "), undefined, data);
		return false;
	}
	rp.dev(`prompt:\n${args.join(" ")}`);
	rp.homebrew(args.join(" "), data);
	return false;
}

function handleGptCommand(args, data) {
	if (!rp.gptPrompt) return true;
	if (args.length < 1) {
		rp.chatHelp();
		return false;
	}
	rp.dev(`prompt:\n${args.join(" ")}`);
	rp.gptPrompt(args.join(" "), data);
	return false;
}

export { registerRenderSettingsConfigHook, registerChatMessageHook };
