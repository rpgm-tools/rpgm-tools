const rp = CONFIG.rp;

function initializeHandlebars() {
	Handlebars.registerHelper("stringify", function (context) {
		return JSON.stringify(context);
	});

	Handlebars.registerHelper("formatDate", function (date) {
		const d = new Date(date);
		const month = String(d.getMonth() + 1).padStart(2, '0');
		const day = String(d.getDate()).padStart(2, '0');
		const year = d.getFullYear();
		return `${year}-${month}-${day}`;
	});

	Handlebars.registerHelper("getLanguages", function () {
		return rp.variables.languages;
	});

	Handlebars.registerHelper("getGenres", function () {
		return rp.variables.genres;
	});

	Handlebars.registerHelper("eq", function (v1, v2) {
		return v1 === v2;
	});

	Handlebars.registerHelper("Log", function (value) {
		rp.log(String(value));
	});

	Handlebars.registerHelper("upperCaseFirst", function (str) {
		if (typeof str === "string" && str.length > 0) {
			return str.charAt(0).toUpperCase() + str.slice(1);
		}
		return "";
	});

	Handlebars.registerHelper("limitInputValue", function (value, min, max) {
		return Math.min(Math.max(value, min), max);
	});

	Handlebars.registerHelper("unless", function (conditional, options) {
		return !conditional ? options.fn(this) : options.inverse(this);
	});

	Handlebars.registerHelper("notEmpty", function (value) {
		return value && value !== "";
	});

	Handlebars.registerHelper("toUpperCase", function (str) {
		return str.toUpperCase();
	});

	Handlebars.registerHelper("replace", function (str, pattern, replacement) {
		if (typeof str !== "string") return "";
		return str.replace(new RegExp(pattern, "g"), replacement);
	});
}

export { initializeHandlebars };
